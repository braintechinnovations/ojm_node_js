const express = require('express')
const jwt = require('jsonwebtoken')
const cookieparser = require('cookie-parser')
const app = express()

require('dotenv').config()
app.use(cookieparser())

app.listen(process.env.PORT, () => {
    console.log(`Sono in ascolto sulla porta ${process.env.PORT}`)
})

/**
 * Utilizzato per il debug, non rendere raggiungibile in produzione
 */
app.get('/loginexposed', (req, res) => {
    // res.end(process.env.CHIAVE)

    let payload = {
        id: "ABC123456",
        isLogged: true,
        role: "USER"
    }

    let options = {
        expiresIn: '100s'
    }

    let token = jwt.sign(payload, process.env.CHIAVE, options)
    res.send(token)
})

app.get('/login', (req, res) => {
    let numeroSecondi = 100

    let payload = {
        id: "ABC123456",
        isLogged: true,
        role: "USER"
    }

    let options = {
        expiresIn: `${numeroSecondi}s`
    }

    let token = jwt.sign(payload, process.env.CHIAVE, options)

    let configCookie = {
        expires: new Date(Date.now() + numeroSecondi * 1000),
        httpOnly: true,
        secure: false
    }

    res.cookie('cookiecifrato', token, configCookie).send("STAPPOOOOOOOO")

//--------------------------------------------------------------------------

    // let persona = {
    //     nome: "Giovanni",
    //     cognome: "Pace"
    // }
    // res.cookie('studente', JSON.stringify(persona)).send("CIAO GIOVANNI")
})

app.get("/profilo", (req, res) => {
    if(!req.cookies.cookiecifrato){
        res.end("NON ESISTE")
    }
    else{
        try {    
            let payload = jwt.verify(req.cookies.cookiecifrato, process.env.CHIAVE)
            res.json(payload)
        } catch (error) {
            res.end("VALIDAZIONE NON SUPERATA!")
        }

    }
})