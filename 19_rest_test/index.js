const express = require('express')
const app = express()

const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get('/studenti/list', (req, res) => {
    let elenco = [
        {
            nome: "Giovanni",
            cognome: "Pace"
        },
        {
            nome: "Valeria",
            cognome: "Verdi"
        }
    ]

    res.json(elenco)
})