const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

const db = mongoose.connect("mongodb+srv://cicciopasticcio:cicciopasticcio@cluster0.tilrf.mongodb.net/gestioneBlog?retryWrites=true&w=majority", {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})

BlogPost.create(
    {
        title: "Prova di primo titolo",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }, (error, blogpost) => {
        if(!error)
            console.log(blogpost)
        else
            console.log(error)
    }
)