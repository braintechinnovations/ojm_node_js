const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

const db = mongoose.connect("mongodb+srv://cicciopasticcio:cicciopasticcio@cluster0.tilrf.mongodb.net/gestioneBlog?retryWrites=true&w=majority", {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})

// FindAll -  SELECT * FROM BlogPosts
// BlogPost.find({}, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// SELECT * FROM BlogPost WHERE title LIKE "%primo%"
// BlogPost.find({
//     title: /primo/
// }, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// SELECT * FROM BlogPost WHERE title LIKE "Titolo%"
// BlogPost.find({
//     title: /^Titolo/
// }, (error, elenco) => {
//     if(!error)
//         console.log(elenco)
//     else
//         console.log(error)
// })

// SELECT * FROM BlogPost WHERE title LIKE "%Titolo"
BlogPost.find({
    title: /titolo$/
}, (error, elenco) => {
    if(!error)
        console.log(elenco)
    else
        console.log(error)
})