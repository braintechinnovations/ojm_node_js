const http = require('http')
const fs = require('fs')

const port = 4000;
const host = "127.0.0.1"

const homePage = fs.readFileSync("home.html")
const aboutPage = fs.readFileSync("about.html")
const contactusPage = fs.readFileSync("contactus.html")

const server = http.createServer((req, res) => {
    switch(req.url){
        case "/":
            res.end(homePage)
            break;
        case "/about":
            res.end(aboutPage)
            break;
        case "/contactus":
            res.end(contactusPage)
            break
        default:
            res.statusCode = 404;
            res.end("Ooops... pagina non trovata!")
    }
})

server.listen(port, host, () => {
    console.log(`server in ascolto su: ${host}:${port}`)
})