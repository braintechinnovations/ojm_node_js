const http = require('http')

const host = "127.0.0.1"
const port = 4001

//Predispongo la reponse
const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain")
    res.end(`Ciao sono il server secondario in ascolto sulla porta ${port}`)

    console.log("Ecco la richiesta pervenuta")
})

server.listen(port, host, () => {
    console.log(`Ciao, sono in ascolto sulla porta ${port}`)
})