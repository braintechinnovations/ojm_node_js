const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrdineSchema = new Schema({
    nom: String,
    ind: String,
    pro: String
})

const Ordine = mongoose.model('Order', OrdineSchema)

module.exports = Ordine;