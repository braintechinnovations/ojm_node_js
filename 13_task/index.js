const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const Ordine = require('./models/Ordine')
const app = express()
const ENV_ONLINE = false;

app.use(express.static('public'))           //Utilizzo un Middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.set('view engine', 'ejs')               //Setup di una impostazione

const port = 4000
const username = "cicciopasticcio"
const password = "cicciopasticcio"
const dbName = "gestioneOrdini"

let connString = `mongodb://localhost:27017/${dbName}?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`
if(ENV_ONLINE){
    console.log("Scelto DB ONLINE")
    connString = `mongodb+srv://${username}:${password}@cluster0.tilrf.mongodb.net/${dbName}?retryWrites=true&w=majority`;
}

const db = mongoose.connect(connString, {useNewUrlParser: true}, () => {
    console.log("Sono connesso!")
})


app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//ROTTE:
app.get('/', (req, res) => {
    res.render('home')
})

app.post('/salva', async (req, res) => {

    let ordine = {
        nom: req.body.nominativo,
        ind: `${req.body.indirizzo} ${req.body.civico}, ${req.body.cap} - ${req.body.citta}`,
        pro: req.body.prodotto
    }

    try {
        let risultato = await Ordine.create(ordine);

        if(risultato){
            res.redirect("/elenco")
        }
        else{
            res.end("Errore di scrittura sul DB")
        }
    } catch (error) {
        console.log(error)
        res.end("Errore del server!")                   //Attivato solo in caso di EXCEPTION
    } finally {                 
        console.log("Questo è il finally")              //Attivato SEMPRE alla fine dell'operazione
    } 

})

app.get('/elenco', async (req, res) => {
    try {
        let risElenco = await Ordine.find({})         //Equivalente di findAll (senza filtro)

        console.log(risElenco)

        res.render('page_elenco', {
            elenco: risElenco
        })
    } catch (error) {
        res.end("Errore del server!") 
    }
})

app.get('/ordini/:idOrdine', async (req, res) => {
    try {
        let risOrdine = await Ordine.findById(req.params.idOrdine)

        res.render('page_ordine', {
            ordine: risOrdine
        })

    } catch (error) {
        console.log(error)
        res.end("Errore del server!") 
    }

})

// app.post('/dettaglio', async (req, res) => {

//     try {
//         let risOrdine = await Ordine.findById(req.body.id_ordine)
//         console.log("CIAO CICCIO")
//         console.log(risOrdine)

//         res.render('page_ordine', {
//             ordine: risOrdine
//         })

//     } catch (error) {
//         console.log(error)
//         res.end("Errore del server!") 
//     }

// })