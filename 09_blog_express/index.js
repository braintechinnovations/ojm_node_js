const express = require('express')
const app = express()
const port = 4000

app.use(express.static('public'))
app.set('view engine', 'ejs')                           //Imposto EJS come View Engine in Express

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/", (req, res) => {
    res.render('index')
})

app.get("/about", (req, res) => {
    res.render('about')
})

app.get("/contact", (req, res) => {
    res.render('contact')
})

app.get("/post", (req, res) => {
    res.render('post')
})

