const mongoose =  require('mongoose')
const Schema = mongoose.Schema

const StudentSchema = new Schema({
    matricola: Number,
    nome: String,
    cognome: String
})

const Student = mongoose.model('Student', StudentSchema)

module.exports = Student