const express = require('express')
const app = express()
const port = 4000

app.use(express.static('public'))
app.set('view engine', 'ejs')

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))

const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

const username = "cicciopasticcio"
const password = "cicciopasticcio"
const dbName = "gestioneBlog"

mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.tilrf.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})


app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//Rotte
app.get("/about", (req, res) => {
    res.render('about');
})
app.get("/contact", (req, res) => {
    res.render('contact');
})
app.get("/post", (req, res) => {
    res.render('post');
})
app.get("/blogpost/new", (req, res) => {
    res.render('create');
})

// app.post("/blogpost/save", (req, res) => {
//     let nuovoBlogPost = {
//         title: req.body.inputTitle,         //POsso accedere alle proprietà grazie al modulo body-parser
//         body: req.body.inputBody
//     }

//     BlogPost.create(nuovoBlogPost, (err, bp) => {
//         if(!err)
//             res.redirect("/")
//         else
//             res.end("Errore di inserimento")
//     })
// })

// app.get("/", (req, res) => {
//     BlogPost.find({}, (error, elenco) => {
//         console.log("Ho finito l'esecuzione su Atlas")
        
//         console.log(elenco)
//         res.render('index');
//     })
// })

app.get("/", async (req, res) => {          //Dichiaro la arrow function come asincrona
    try {
        let elenco = await BlogPost.find({})    //Attendi il risultato della find e non vai avanti finché non risponde!    

        console.log(elenco)
        res.render('index', {
            elencoarticoli: elenco              //Passo un oggetto al mio EJS
        })
    } catch (error) {
        res.end("Errore del server")
    }
})

app.get("/blogpost/:postId", async (req, res) => {
    try {
        let articolo = await BlogPost.findById(req.params.postId)

        res.render('post', {
            blogpost: articolo
        })
    } catch (error) {
        res.end("Errore del server")
    }
})

const fileUpload = require('express-fileupload')
app.use(fileUpload())

const path = require('path')

app.post("/blogpost/save", async (req, res) => {

    let img = req.files.inputFile
    if(img.size > 100000){
        res.end("Immagine troppo grande!")
    }

    //Se voglio un caricamento sequenziale, aggiungere: (new Date()).getTime() e concatenarlo al nome immagine
    img.mv(path.resolve(__dirname, "public/img", img.name), async (error) => {
        if(!error){
            let nuovoBlogPost = {
                title: req.body.inputTitle,
                body: req.body.inputBody, 
                image: img.name
            }

            let nuovoPost = await BlogPost.create(nuovoBlogPost)

            if(!nuovoPost){
                res.end("Errore di inserimento")
            }
            else{
                res.redirect(`/blogpost/${nuovoPost._id}`)
            }
        }
        else{
            res.end("Errore di caricamento")
        }
    })

    // res.json(req.files.inputFile)

    // let nuovoBlogPost = {
    //     title: req.body.inputTitle,
    //     body: req.body.inputBody
    // }

    // BlogPost.create(nuovoBlogPost, (err, bp) => {
    //     if(!err)
    //         res.redirect("/")
    //     else
    //         res.end("Errore di inserimento")
    // })
})