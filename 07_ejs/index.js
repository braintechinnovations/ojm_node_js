const express = require('express')
const app = express()
const port = 4000

app.use(express.static('public'))
app.set('view engine', 'ejs')                           //Imposto EJS come View Engine in Express

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//ROTTE
app.get("/", (req, res) => {
    res.render('home')
})

app.get("/chisiamo", (req, res) => {
    res.render('chi_siamo')
})

app.get("/servizi", (req, res) => {
    res.render('servizi')
})

app.get("/contatti", (req, res) => {
    res.render('contatti')
})