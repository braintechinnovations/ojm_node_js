const Subscription = require('../models/Subscription')

module.exports = (req, res) => {
    Subscription.findByIdAndDelete(req.params.id, (err) => {
        if (!err) {
            res.redirect('/subscriptions')
        } else {
            res.render('error', {
                details: err._message
            })
        }
    })
}