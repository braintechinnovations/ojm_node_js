const Order = require("../models/Order")

module.exports = async (req, res) => {
    let idUtente = req.session.userId
    let idServizio = req.params.idSubs

    let ordine = {
        user: idUtente,
        subs: idServizio,
        paga: req.body.inputPagamento,
        note: req.body.inputNote
    }

    try {
        let ordineCreato = await Order.create(ordine)
        
        res.redirect(`/order/${ordineCreato._id}`)
    } catch (error) {
        res.render('error', {
            details: err._message
        })
    }

}