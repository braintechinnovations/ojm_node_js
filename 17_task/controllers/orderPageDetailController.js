const Order = require("../models/Order")

module.exports = async (req, res) => {
    try {
        let ordine = await Order.findOne({
            "_id": req.params.idOrder,
            "user": req.session.userId
        })
        console.log("SONO QUIIIIII")

        console.log(ordine)

        if(ordine)
            res.json(ordine)
        else
            res.end("Non autorizzato oppure ordine non trovato")
    } catch (error) {
        res.end(error._message)
    }
}