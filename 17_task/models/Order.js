const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrderSchema = new Schema(
    {
        user: String,
        subs: String,
        data: {
            type: Date,
            default: new Date()
        },
        paga: String,
        note: String
    }
)

const Order = mongoose.model('Order', OrderSchema)
module.exports = Order