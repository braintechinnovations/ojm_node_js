const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const expressSession = require('express-session')
const fileUpload = require('express-fileupload')
const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(fileUpload())
app.use(expressSession(
    {
        secret: "sonomrmiguardi",
        resave: false,
        saveUninitialized: false,
        cookie: {
            secure: 'auto',
            maxAge: 3600000
        }
    }
))
app.set('view engine', 'ejs')

const porta = 4000
const username = "cicciopasticcio"
const password = "cicciopasticcio"
const dbName = "gestioneServizi"

//Connessione al DB
mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.tilrf.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})

app.listen(porta, () => {
    console.log(`Sono in ascolto sulla porta ${porta}`)
})

global.loggato = null
global.ruolo = null

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    ruolo = req.session.role

    console.log(loggato, ruolo)
    next()
})

//ROTTE
const homeController = require('./controllers/homeController')
const registerPageController = require('./controllers/registerPageController')
const registerActionController = require('./controllers/registerActionController')
const loginPageController = require('./controllers/loginPageController')
const loginActionController = require('./controllers/loginActionController')
const logoutController = require('./controllers/logoutController')
const subsPageAddController = require('./controllers/subsPageAddController')
const subsPageActionController = require('./controllers/subsPageActionController')
const subsPageListController = require('./controllers/subsPageListController')
const subsPageDeleteController = require('./controllers/subsPageDeleteController')
const subsPageEditController = require('./controllers/subsPageEditController')

const checkAdminMiddleware = require('./middleware/checkAdminMiddleware')
const subsPageEditActionController = require('./controllers/subsPageEditActionController')
const checkUserMiddleware = require('./middleware/checkUserMiddleware')
const userSubsBuyController = require('./controllers/userSubsBuyController')
const userSubsBuyActionController = require('./controllers/userSubsBuyActionController')
const orderPageDetailController = require('./controllers/orderPageDetailController')

app.get("/", homeController)
app.get("/auth/register", registerPageController)
app.post("/auth/register", registerActionController)
app.get("/auth/login", loginPageController)
app.post("/auth/login", loginActionController)
app.get("/auth/logout", logoutController)

app.get("/subscriptions/add", checkAdminMiddleware, subsPageAddController)
app.post("/subscriptions/add", checkAdminMiddleware, subsPageActionController)
app.get("/subscriptions", checkAdminMiddleware, subsPageListController)
app.get("/subscriptions/delete/:id", checkAdminMiddleware, subsPageDeleteController)
app.get("/subscriptions/edit/:id", checkAdminMiddleware, subsPageEditController)
app.post("/subscriptions/edit/:id", checkAdminMiddleware, subsPageEditActionController)

app.get("/user/subscriptions", checkUserMiddleware, subsPageListController)
app.get("/user/subscriptions/buy/:idSubs", checkUserMiddleware, userSubsBuyController)
app.post("/user/subscriptions/buy/:idSubs", checkUserMiddleware, userSubsBuyActionController)

app.get("/order/:idOrder", orderPageDetailController)