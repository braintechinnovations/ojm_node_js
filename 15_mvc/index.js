const express = require('express')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const mongoose = require('mongoose')
const path = require('path')
const BlogPost = require('./models/BlogPost')

const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.use(fileUpload())
app.set('view engine', 'ejs')

const port = 4001
const username = "cicciopasticcio"
const password = "cicciopasticcio"
const dbName = "gestioneBlog"

//Connessione al DB
mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.tilrf.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})
//Avvio server Express
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//ROTTE
const homeController = require('./controllers/homeController')
const postDetailController = require('./controllers/postDetailController')
const postNewController = require('./controllers/postNewController')
const postSaveController = require('./controllers/postSaveController')

app.get("/", homeController)
app.get("/blogpost", postNewController)             //Necessario dichiarlo prima della variabile sottostante
app.get("/blogpost/:postId", postDetailController)
app.post("/blogpost", postSaveController)