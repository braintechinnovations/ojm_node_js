const BlogPost = require('../models/BlogPost')

module.exports = async (req, res) => {
    try {
        let articolo = await BlogPost.findById(req.params.postId)

        res.render('post', {
            blogpost: articolo
        })
    } catch (error) {
        res.end("Errore del server (sto nel dettaglio)")
    }
}