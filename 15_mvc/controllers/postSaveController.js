const BlogPost = require('../models/BlogPost')
const path = require('path')

module.exports = async (req, res) => {

    let img = req.files.inputFile
    if(img.size > 100000){
        res.end("Immagine troppo grande!")
    }

    img.mv(path.resolve(__dirname, "..", "public/img", img.name), async (error) => {
        if(!error){
            let nuovoBlogPost = {
                title: req.body.inputTitle,
                body: req.body.inputBody, 
                image: img.name
            }

            let nuovoPost = await BlogPost.create(nuovoBlogPost)

            if(!nuovoPost){
                res.end("Errore di inserimento")
            }
            else{
                res.redirect(`/blogpost/${nuovoPost._id}`)
            }
        }
        else{
            res.end("Errore di caricamento")
        }
    })
}