const express = require("express")
const path = require("path")
const app = express()

app.use(express.static('public'))

const port = 4000;
app.listen(port, () => {
    console.log(`In ascolto sulla ${port}`)
})

app.get("/", (req, res) => {
    console.log(__dirname)
    console.log(path.resolve(__dirname, "pages", "home.html"))

    res.sendFile(
        path.resolve(__dirname, "pages", "home.html")
    )
})


app.get("/chisiamo", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "pages", "chi_siamo.html")
    )
})

app.get("/contatti", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "pages", "contatti.html")
    )
})

app.get("/servizi", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "pages", "servizi.html")
    )
})


