const express = require('express')
const app = express()
const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto alla porta ${port}`)
})

// rotte e metodi
app.get("/", (req, res) => {
    let responso = {
        pagina: "principale"
    }

    res.json(responso)
})

app.get("/about", (req, res) => {
    let responso = {
        pagina: "chisiamo"
    }

    res.json(responso)
})

app.get("/contactus", (req, res) => {
    let responso = {
        pagina: "contatti"
    }

    res.json(responso)
})