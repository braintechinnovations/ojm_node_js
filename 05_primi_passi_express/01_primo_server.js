// entro nella directory del progetto
// > npm init
// Seguo la procedura (tanti invio)
// > npm install express

// const http = require('http')
// const port = 4000
// const host = "127.0.0.1";

// const server = http.createServer((req, res) => {
//     // ...
// })

// server.listen(port, host, () => {
//     // ...
// })

const express = require('express')

const app = express()                   //Inizializzazione di express

const port = 4000
const host = "127.0.0.1";

app.get("/", (req,res) => {
    res.end("Sono la pagina principale")
})
 
app.get("/about", (req,res) => {
    res.end("Sono la pagina About")
})
 
app.get("/contactus", (req, res) => {
    res.end("Sono la pagina Contatti")
})

app.listen(port, host, () => {
    console.log(`Server disponibile alla porta ${port}`)
})