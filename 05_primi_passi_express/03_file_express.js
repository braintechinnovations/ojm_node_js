const express = require('express')
const path = require('path')                    //Modulo per la risoluzione degli indirizzi
const app = express()
const port = 4000;

app.use(express.static('public'))

app.listen(port, () => {
    console.log(`In ascolto sulla ${port}`)
})

app.get("/", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "03_home.html")    
    )
})

app.get("/about", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "03_about.html")    
    )
})

app.get("/contactus", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "03_contactus.html")    
    )
})

/*
Creare un piccolo Front-End con NodeJS introducendo 3 tipologie diverse di pagine ed un menu di navigazione
Home page
Chi siamo
Servizi
Contatti
*/