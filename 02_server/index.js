const http = require('http')                //Importa il modulo HTTP esportato in precedenza

const port = 4000;
const host = "127.0.0.1"

const server = http.createServer((req, res) => {
    // console.log(req.url)
    // console.log(req.method)

    let responso = {
        indirizzo: req.url,
        metodo: req.method
    }
    
    res.setHeader("Content-Type", "text/json")
    res.end(JSON.stringify(responso))
})

server.listen(port, host, () => {
    console.log(`In ascolto sulla porta ${port}`)
});