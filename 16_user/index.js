const express = require('express')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const mongoose = require('mongoose')
const expressSession = require('express-session')

const app = express()

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extender: true}))
app.use(fileUpload())
app.use(expressSession({
    secret: "sonomrmiguardi",
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: 'auto',
        maxAge: 3600000
    }
}))

app.set('view engine', 'ejs')

const port = 4000
const username = "cicciopasticcio"
const password = "cicciopasticcio"
const dbName = "gestioneBlog"

//Connessione al DB
mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.tilrf.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
})
//Avvio server Express
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

//VARIABILI GLOBALI
global.loggato = null;

app.use("*", (req, res, next) => {
    loggato = req.session.userId
    // console.log(loggato)

    // if(loggato){
    //     User.findById(loggato, (err, userFound) => {
    //         console.log(err, userFound)
    //         if(!userFound){
    //             console.log("Sono nel ramo di disattivazione!")
    //             loggato = ""
    //         }
    //     })
    // }

    next()
})

//ROTTE
const homeController = require('./controllers/homeController')
const postDetailController = require('./controllers/postDetailController')
const postNewController = require('./controllers/postNewController')
const postSaveController = require('./controllers/postSaveController')
const authRegisterController = require('./controllers/authRegisterController')
const authSaveController = require('./controllers/authSaveController')
const authLoginPageController = require('./controllers/authLoginPageController')
const authLoginActionController = require('./controllers/authLoginActionController')
const redirectIfNotLogged = require('./middleware/redirectIfNotLogged')
const User = require('./models/User')
const authLogoutController = require('./controllers/authLogoutController')

app.get("/", homeController)
app.get("/blogpost", redirectIfNotLogged ,postNewController)             //Necessario dichiarlo prima della variabile sottostante
app.get("/blogpost/:postId", postDetailController)
app.post("/blogpost", redirectIfNotLogged, postSaveController)

app.get("/auth/register", authRegisterController)
app.post("/auth/save", authSaveController)
app.get("/auth/login", authLoginPageController)
app.post("/auth/login", authLoginActionController)
app.get("/auth/logout", authLogoutController)

app.use((req, res) =>{
    res.end("Pagina non trovata");
})

/**
 * TASK:
 * 
 * Creare un piccolo sistema di gestione bacheca annunci, ogni annuncio sarà caratterizzato da:
 * - Nome dell'annuncio
 * - Prezzo dell'annuncio
 * - Descrizione dell'annuncio
 * - Foto del annuncio
 * - Data di pubblicazione dell'annuncio
 * - Utente che ha pubblicato l'annuncio
 * 
 * 1. Creare un sistema di navigazione che ci porti tra le varie funzionalità:
 * HOME - Elenco prodotti
 * NUOVO PRODOTTO - Inserimento nuovo prodotto
 * REGISTRAZIONE - Nuovo utente
 * LOGIN - Login utente
 * 
 * 2. Creare un sistema di CR-UD degli annunci (P.S. inventiamo qualcosa per Update e Delete che può essere fatto anche dopo!)
 * 3. Creare un sistema di Registrazione e Login dell'utente
 * 4. Obbligare il login prima dell'inserimento annuncio! 
*/  