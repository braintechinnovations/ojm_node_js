const BlogPost = require('../models/BlogPost')

module.exports = async (req, res) => {          //Dichiaro la arrow function come asincrona
    // console.log(req.session)

    try {
        let elenco = await BlogPost.find({})    //Attendi il risultato della find e non vai avanti finché non risponde!    

        res.render('index', {
            elencoarticoli: elenco              //Passo un oggetto al mio EJS
        })
    } catch (error) {
        res.end("Errore del server (sto nella home)")
    }
}