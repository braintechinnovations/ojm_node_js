const User = require("../models/User")

module.exports = async (req, res) => {
    let utente = {
        username: req.body.inputUser,
        password: req.body.inputPassword
    }

    try {
        let userSaved = await User.create(utente);

        if(!userSaved){
            res.end("Errore di inserimento utente")
        }
        else{
            res.render('status_ok', {
                pageTitle: "Tutto ok!",
                pageBody: "Utente creato con successo!"
            })
        }
    } catch (error) {
        res.end("Errore del server ;(")
    }
    
}