module.exports = (req, res) => {
    req.session.destroy(() => {
        console.log("Sessione distrutta")
        res.redirect('/')
    })
}