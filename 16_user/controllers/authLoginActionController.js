const User = require("../models/User")
const bcrypt = require('bcrypt')

module.exports = (req, res) => {
 
    let utente = {
        username: req.body.inputUser,
        password: req.body.inputPassword
    }

    User.findOne(
        {
            username: utente.username
        }, (err, userResult) => {           //Cerco l'utente sul DB, se esiste lo mette dentro a userResult
            if(!err && userResult){

                bcrypt.compare(utente.password, userResult.password, (err, same) => {       //Comparo le password in modo da verificare se quella che gli ho passato è identica a quella sul DB
                    if(same){
                        req.session.userId = userResult._id         //Salvo in sessione SOLO lo user ID!
                        res.redirect('/')
                    }
                    else{
                        res.redirect('/auth/login')
                    }
                })

            }
            else{
                console.log(err)
                res.redirect('/auth/login')
            }
        }
    )

}