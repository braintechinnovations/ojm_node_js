const User = require('../models/User')

module.exports = (req, res, next) => {
    if(req.session.userId){
        User.findById(req.session.userId, (err, userFound) => {
            if(!err && userFound){
                next()
            }
            else{
                req.session.userId = null;
                res.redirect('/auth/login')
            }
        })
        
    }
    else{
        res.redirect('/auth/login')
    }
}