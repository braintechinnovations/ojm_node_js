const http = require('http')

const port = 4000;
const host = "127.0.0.1"

const server = http.createServer((req, res) => {
    switch(req.url){
        case "/":
            res.end("Sono la pagina HOME")
            break;
        case "/about":
            res.end("Sono la pagina CHI SIAMO")
            break;
        case "/contactus":
            res.end("Sono la pagina CONTATTI")
            break
        default:
            res.statusCode = 404;
            res.end("Ooops... pagina non trovata!")
    }
})

server.listen(port, host, () => {
    console.log(`server in ascolto su: ${host}:${port}`)
})