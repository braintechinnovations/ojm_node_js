const express = require('express')
const app = express()
const port = 4000

app.use(express.static('public'))
app.set('view engine', 'ejs')                           //Imposto EJS come View Engine in Express

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/", (req, res) => {
    let responso = {
        status: "Success"
    }

    res.json(responso);
})


app.get("/about", (req, res) => {
    let responso = {
        status: "Success"
    }

    res.json(responso);
})