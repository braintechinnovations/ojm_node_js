const pg = require('pg')
const Pool = pg.Pool

const pool = new Pool({
    user: "postgres",
    password: "toor",
    database: "rubrica",
    host: "localhost",
    port: 5432
})

module.exports = pool