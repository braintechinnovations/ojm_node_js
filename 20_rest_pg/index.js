const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./dbConfig')
const cors = require('cors')                //Dopo aver fatto "npm i cors"
const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({extender: true}))

const port = 4000

app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/persone/list", async (req, res) => {
    try {
        let elenco = await pool.query('SELECT * FROM persone');

        console.log(`Numero di righe: ${elenco.rowCount}`)

        res.json(elenco.rows)
    } catch (error) {
        console.log(error)
    }
})

app.get("/persone/:idPersona", async (req, res) => {
    try {
        // let risultato = await pool.query(`SELECT * FROM persone WHERE id = ${req.params.idPersona}`)
        let risultato = await pool.query('SELECT * FROM persone WHERE id = $1',  [req.params.idPersona])

        res.json(risultato.rows[0])
    } catch (error) {
        console.log(error)
    }
})

app.post("/persone/insert", async (req, res) => {
    try {
        let querySql = `INSERT INTO persone (nome, cognome, telefono)` + 
            `VALUES ('${req.body.nome}', '${req.body.cognome}', '${req.body.telefono}')`;

        let risultatoInserimento = await pool.query(querySql)
        
        if(risultatoInserimento.rowCount > 0){
            res.json({status: "success"})
        }
        else{
            res.json({status: "error"})
        }
    } catch (error) {
        console.log(error)
    }
})

app.delete("/persone/:idPersona", async (req, res) => {
    try {
        let esitoOperazione = await pool.query(`DELETE FROM persone WHERE id = ${req.params.idPersona}`)

        if(esitoOperazione.rowCount > 0){
            res.json({status: "eliminato con successo!"})
        }
        else{
            res.json({status: "errore, dato non trovato"})
        }
    } catch (error) {
        res.json({status: error._message})
    }
})

app.put("/persone/:idPersona", async (req, res) => {
    try {
        let oggettoPreSalvataggio = await pool.query(`SELECT * FROM persone WHERE id = ${req.params.idPersona}`)
        personaPreSalvataggio = oggettoPreSalvataggio.rows[0]

        if(req.body.nome)
            personaPreSalvataggio.nome = req.body.nome
        if(req.body.cognome)
            personaPreSalvataggio.cognome = req.body.cognome
        if(req.body.telefono)
            personaPreSalvataggio.telefono = req.body.telefono

        //UPDATE

        let queryUpdate = `UPDATE persone SET ` +
                            `nome = '${personaPreSalvataggio.nome}', ` + 
                            `cognome = '${personaPreSalvataggio.cognome}', ` + 
                            `telefono = '${personaPreSalvataggio.telefono}' WHERE id = ${req.params.idPersona}`

        let risultatoUpdate = await pool.query(queryUpdate)

        if(risultatoUpdate.rowCount > 0){
            res.json({status: "modificato con successo!"})
        }
        else{
            res.json({status: "errore, modifica non effettuata"})
        }
    } catch (error) {
        console.log(error)
    }
})

/**
 * Sviluppare una semplice REST API che si connette al DB di tipo PostgreSQL ed effettua delle CRUD di pietanze:
 * Una pietanza è caratterizzata da:
 * - Nome
 * - Descrizione
 * - Prezzo
 * - Senza glutine? (true/false)
 */